Installation
------------

To get it functional, copy the box.png and box-borders.png to the theme directory.
In the element for which you want themed borders, such as block.tpl.php, place this code at the start

<?php
print drop_box_start('box', array('color' = 0xFFFFF));
?>

and at the end

<?php
print drop_box_finish('box');
?>

The name at the start of the functions references the box.png and box-borders.png, so if you want multiple types, 
simply create new filenames as {$name}.png and {$name}-borders.png. Also, if you want the module to function with 
IE6 and hopefully before, you'll need to create box.gif, and box-borders.gif. This was done so that opacity would 
function for those browsers which allowed it, and simple transparency for those which could not.

To add a title for the box, simply add within array('color' => colorcode, 'title'=> 'Title here').

With this you should be able to do fancier drop boxes.

This is a very basic module right now. It would be possible to add more functions, but it couldn't think of them and 
really have a heavy time schedule right now which precludes me from adding much more. However, if you make suggestions, 
I can always add :).